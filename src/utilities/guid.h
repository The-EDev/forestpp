#pragma once
#include <string>
#include <vector>


namespace GUID{

    std::string GUIDToString(const std::vector<std::uint8_t>& guid);

    std::string rawBinToString(const std::vector<std::uint8_t>& bin);
	
    std::vector<std::uint8_t> stringToGUID(std::string str);

    std::vector<std::uint8_t> generateGUID();
}
