#include <random>
#include <sstream>
#include <string>
#include <algorithm>
#include "guid.h"

namespace GUID
{

    inline std::uint8_t getHexInt(char& item)
    {
        if (std::toupper(item) < 65)
            return item-48; //48 is the ASCII code for the number 0
        else
            return item-55; //65 is the ASCII code for the letter A. Since A represents 10, only 55 are subtracted from the ASCII code
    }
    //get an 8 bit uint from 2 hex characters
    inline std::uint8_t getHexByte(char& big, char& small)
    {
        std::uint8_t bigFound = getHexInt(big);
        std::uint8_t smallFound = getHexInt(small);

        return (bigFound<<4)+smallFound;
    }

    //Turn a string GUID (with or without dashes) to an array of bytes
    std::vector<std::uint8_t> stringToGUID(std::string str)
    {
        std::vector<std::uint8_t> toReturn(16);

        str.erase(std::remove(str.begin(), str.end(), '-'), str.end());

        for (int i = 0; i<32; i+=2)
        {
            toReturn[i/2] = getHexByte(str[i], str[i+1]);
        }
        return toReturn;
    }

    //Represent Binary data as hex characters (without dashes)
    std::string rawBinToString(const std::vector<std::uint8_t>& bin)
    {
        static const char digits[17] = "0123456789ABCDEF";

        char toReturn[33];
        unsigned int j = 0;

        for (unsigned int i = 0; i<16; i++)
        {
            std::uint8_t byte = bin[i];
            std::uint8_t big = byte/16;
            std::uint8_t small = byte%16;
            toReturn[j++] = digits[big];
            toReturn[j++] = digits[small];
        }
        return std::string(toReturn).substr(0,32);
    }

    //Represent the binary GUID as a string of hex characters (with dashes)
    std::string GUIDToString(const std::vector<std::uint8_t>& guid){
        static const char digits[17] = "0123456789ABCDEF";

        char toReturn[37];
        unsigned int j = 0;

        for (unsigned int i = 0; i<16; i++)
        {
            if (j == 8 || j == 13 || j == 18 || j == 23)
                toReturn[j++] = '-';
            std::uint8_t byte = guid[i];
            std::uint8_t big = byte/16;
            std::uint8_t small = byte%16;
            toReturn[j++] = digits[big];
            toReturn[j++] = digits[small];
        }
        return std::string(toReturn).substr(0,36);
    }

    //Generate a version 4 UUID represented in 16 bytes (as a list of 16 bytes or unsigned 8bit integers)
    //HEX
    //(0-15)*8-(0-15)*4-4(0-15)*3-(8-11)(0-15)*3-(0-15)*12
    //
    //BIN
    //(0-255)*4-(0-255)*2-(64-79)(0-255)-(128-191)(0-255)-(0-255)*6
    std::vector<std::uint8_t> generateGUID(){
        static std::random_device                          rd;
        static std::mt19937                                gen(rd());
        static std::uniform_int_distribution<unsigned int> dis(0, 0xFFFFFFFF);
        static std::uniform_int_distribution<>             dis2(64, 79); //0x40 - 0x4F (for UUIDv4)
        static std::uniform_int_distribution<>             dis3(128, 191);//0x80 - 0xBF  (for variant 1) [0x8 = (10)00 and 0xB = (10)11, (10) indicating variant 1]

        //This is how generating is working:
        //1. Generate a 4 byte (32 bit) unsigned integer
        //2. Take the most significant byte and isolate it by shifting 24 bits right to make it 0-255
        //3. Take the byte after that by ANDing the original number with 0x00FFFFFF to remove the most significant byte, then shift again
        //4. Repeat 3 until the last byte is reached
        //5. The last byte only needs to be isolated (by ANDing with 0s everywhere else), no shifting is needed.
        //
        //There are exceptions for generating the version (4) and variant(1) numbers

        std::vector<std::uint8_t> toReturn(16);
        unsigned int x = dis(gen);
        toReturn[0] = (x) >> 24;
        toReturn[1] = (x&0x00FF0000) >> 16;
        toReturn[2] = (x&0x0000FF00) >> 8;
        toReturn[3] = (x&0x000000FF);
        x = dis(gen);
        toReturn[4] = (x) >> 24;
        toReturn[5] = (x&0x00FF0000) >> 16;
        toReturn[6] = (dis2(gen));
        toReturn[7] = (x&0x0000FF00) >> 8;
        toReturn[8] = (dis3(gen));
        toReturn[9] = (x&0x000000FF);
        x = dis(gen);
        toReturn[10] = (x) >> 24;
        toReturn[11] = (x&0x00FF0000) >> 16;
        toReturn[12] = (x&0x0000FF00) >> 8;
        toReturn[13] = (x&0x000000FF);
        x = dis(gen);
        toReturn[14] = (x&0x0000FF00) >> 8;
        toReturn[15] = (x&0x000000FF);
            return toReturn;
    }
}
