#pragma once
#include <string>

namespace Hash{
	std::string run_hkdf(std::string plaintext);
}
