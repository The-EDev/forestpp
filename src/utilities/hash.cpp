#include "hash.h"
#include <cryptlib.h>
#include <hkdf.h>
#include <sha.h>
#include <filters.h>
#include <hex.h>
#include <sstream>

std::string Hash::run_hkdf(std::string plaintext){
	size_t plen = plaintext.length();
	
	CryptoPP::byte* passwd = (unsigned char*)plaintext.c_str();
	
    CryptoPP::byte salt[] = "I am Salty AF and this keeps my passwords relatively secure";
	size_t slen = strlen((const char*)salt);
	
	CryptoPP::byte info[] = "HKDF key derivation";
	size_t ilen = strlen((const char*)info);
	
	CryptoPP::byte derived[CryptoPP::SHA512::DIGESTSIZE];
	
	CryptoPP::HKDF<CryptoPP::SHA512> hkdf;
	hkdf.DeriveKey(derived, sizeof(derived), passwd, plen, salt, slen, info, ilen);
	
	std::string result;
	CryptoPP::HexEncoder encoder(new CryptoPP::StringSink(result));
	
	encoder.Put(derived, sizeof(derived));
	encoder.MessageEnd();
	return result;
}
