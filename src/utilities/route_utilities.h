#define CROW_JSON_USE_MAP

#pragma once
#include <sqlpp11/sqlpp11.h>
#include <sqlpp11/sqlite3/sqlite3.h>
#include <vector>

#include "crow_all.h"
#include "token.h"
#include "guid.h"

namespace RouteUtilities{
inline crow::json::wvalue responseSuccess(std::vector<std::string>& spuriousParams, std::string message = "")
{
    crow::json::wvalue toReturn;
    toReturn["spurious parameters"] = spuriousParams;
    toReturn["msg"] = message;
    toReturn["success"] = true;
    return toReturn;
}

inline crow::json::wvalue responseDataSuccess(const crow::json::wvalue& data, std::vector<std::string>& spuriousParams, std::string message = "")
{
    crow::json::wvalue toReturn(responseSuccess(spuriousParams, message));
    toReturn["count"] = data.size();
    toReturn["data"] = crow::json::wvalue(data); //TODO this probably needs to be a list, and have a separate thing for single returns
    return toReturn;
}

inline crow::json::wvalue responseFailure(std::string message)
{
    crow::json::wvalue toReturn;
    toReturn["success"] = false;
    toReturn["msg"] = message;
    return toReturn;
}

inline std::string parse_query_param(const std::string& input, const std::string& name)
{
    if (input == "")
        return "";
    std::string toReturn(name + '.');
    toReturn.reserve(input.size());
    for (char x : input)
    {
        switch (x)
        {
            case ',':
                toReturn += " AND " + name + '.';
                break;

            case '\'':
                break;

            case ';':
                break;

            default:
                toReturn += x;
                break;
        }
    }
    return toReturn;
}

    template <typename T>
    inline std::string get_name()
    {
        //Alias Substruct, take it's name function, and get the char* from that, then put the type of the table or member in there. and you got yourself a char* of the name.
        return std::string(typename T::_alias_t::_name_t().template char_ptr<T>());
    }

    template <typename T>
    inline void tableGet(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, T& table)
    {
        Token::verifyToken(req, res, db);

        crow::query_string params (req.url_params);
        char* querystring = params.pop("query");
        std::string where (parse_query_param(std::string(querystring != nullptr ? querystring : ""), get_name<T>()));

        auto items = (where == "" ? db(select(all_of(table)).from(table).unconditionally())
                                  : db(select(all_of(table)).from(table).where(sqlpp::verbatim<sqlpp::boolean>(where))));

        std::vector<crow::json::wvalue> itemJsonList;

        for (const auto& item : items)
        {
            crow::json::wvalue itemJson(T::get_row_as_json(item));
            itemJsonList.emplace_back(itemJson);
        }
        std::vector<std::string> paramKeys = params.keys();

        res = crow::response(responseDataSuccess(crow::json::wvalue(itemJsonList), paramKeys));
        res.end();
    }

    template <typename T>
    inline void tableGetOne(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, T& table, const unsigned int& id)
    {;

        auto tableSelect = db(select(all_of(table)).from(table).where(table.id == id));

        if (tableSelect.empty())
        {
            std::string tableName = get_name<T>();
            res = crow::response(404, (tableName + " not found"));
            res.end();
            return;
        }

        crow::json::wvalue x = T::get_row_as_json(tableSelect.front());

        std::vector<std::string> y = req.url_params.keys();

        crow::json::wvalue xFinal = RouteUtilities::responseDataSuccess(x, y);

        res = crow::response(xFinal);
        res.end();
    }

    template <typename T>
    inline void tablePost(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, T& table)
    {
        crow::json::rvalue data = crow::json::load(req.body);

        std::string columns, values;
        std::vector<crow::json::rvalue> dataList(data.lo());
        for (crow::json::rvalue item : dataList)
        {
            if (std::string(item.key()) != "id")
            {
                columns += std::string(item.key()) + std::string(", ");
                values += std::string(item) + std::string(", ");
            }
        }

        std::vector<std::uint8_t> guid = GUID::generateGUID();
        columns += "guid";
        values += "x'" + GUID::rawBinToString(guid) + "'";

        std::string SQLStatement = "INSERT INTO " + get_name<T>() + " (" + columns + ") VALUES (" + values + ");";

        auto d = custom_query(sqlpp::verbatim(SQLStatement)).with_result_type_of(sqlpp::insert());
        auto j = db(d);

        tableGetOne(req, res, db, table, j);
    }

    template <typename T>
    inline void tableUpdate(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, T& table, const unsigned int& id)
    {
        crow::json::rvalue data = crow::json::load(req.body);

        std::string values;
        std::vector<crow::json::rvalue> dataList(data.lo());
        for (crow::json::rvalue item : dataList)
        {
            if (std::string(item.key()) != "id")
                values += std::string(item.key()) + " = " + std::string(item) + std::string(", ");
        }

        std::string SQLStatement = "SET " + values.substr(0, values.size()-2);
        db(custom_query(update(table), sqlpp::verbatim(SQLStatement), where(table.id == id)).with_result_type_of(sqlpp::update(table)));

        tableGetOne(req, res, db, table, id);
    }

    template <typename T>
    inline void tableDelete(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, T& table, const unsigned int& id)
    {
        auto tableSelect = db(select(table.id).from(table).where(table.id == id));
        if (tableSelect.empty())
        {
            std::string tableName = get_name<T>();
            res = crow::response(404, (tableName + " not found"));
            res.end();
            return;
        }

        db(remove_from(table).where(table.id == id));


        std::string tableName = get_name<T>();
        tableSelect = db(select(table.id).from(table).where(table.id == id));
        if (!tableSelect.empty())
        {
            res = crow::response(500, ("Failed to remove " + tableName));
            res.end();
            return;
        }
        auto x = req.url_params.keys();
        res = crow::response(responseSuccess(x, ("Succesfully removed " + tableName)));
        res.end();
    }
}
