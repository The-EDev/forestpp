#include <random>
#include <string>
#include <sstream>
#include <sqlpp11/sqlpp11.h>
#include "../models/auth_token.h"
#include "token.h"

std::string Token::generateToken(int length)
{
	static std::random_device              rd;
	static std::mt19937_64                 gen(rd());
	static std::uniform_int_distribution<> dis(0, 15);
	
	std::stringstream toReturn;
	toReturn << std::hex;
	for (int i=0; i<length;i++)
	{
		toReturn << dis(gen);
	}
	
	return toReturn.str();
}

//token = `Bearer abc123`
int Token::verifyToken(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, std::string prefix, bool noRes){
    std::string tokenString = req.get_header_value("Authorization");
    if (tokenString.empty())
    {
        if (!noRes)
        {
            res = crow::response(401, "missing token");
            res.end();
        }
        return -1;
    }

    size_t delim = tokenString.find(" ");
    if (delim == std::string::npos || tokenString.substr(0, delim) != prefix)
    {
        if (!noRes)
        {
            res = crow::response(401, "malformed token");
            res.end();
        }
        return -1;
    }

    std::string pureToken = tokenString.substr(delim+1); //+1 to skip the whitespace.

    AuthToken::AuthToken tokenTab;
    auto row = db(select(all_of(tokenTab)).from(tokenTab).where(tokenTab.accessToken == pureToken));

    int currentTime = int(std::time(0));

    if (row.empty() || row.front().revoked)
    {
        if (!noRes)
        {
            res = crow::response(401, "invalid token");
            res.end();
        }
        return -1;
    }
    if (currentTime - row.front().issue > row.front().validFor)
    {
        if (!noRes)
        {
            res = crow::response(401, "invalid token");
            res.end();
        }
        return -1;
    }

    return row.front().id;
}
