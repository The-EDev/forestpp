#define CROW_JSON_USE_MAP

#pragma once
#include <string>
#include <sqlpp11/sqlite3/sqlite3.h>
#include "crow_all.h"

namespace Token{
    int verifyToken(const crow::request& req, crow::response& res, sqlpp::sqlite3::connection& db, std::string prefix = "Bearer", bool noRes = false);
	std::string generateToken(int length = 64);
}
