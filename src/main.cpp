#define CROW_MAIN
#define CROW_JSON_USE_MAP

#include <sqlpp11/sqlpp11.h>
#include <sqlpp11/sqlite3/sqlite3.h>
#include "crow_all.h"
#include "routes/user/user_routes.h"
#include "routes/auth/auth_routes.h"
#include "routes/dbfile/dbfile_routes.h"

int main()
{

	//Crow app initialization
    crow::SimpleApp app;

    sqlpp::sqlite3::connection_config config;
    config.path_to_database = "db";
    config.flags = SQLITE_OPEN_READWRITE;
    config.debug = true;
    sqlpp::sqlite3::connection db(config);

    UserRoutes::getRoutes(app, db);
    AuthRoutes::getRoutes(app, db);
    DBFileRoutes::getRoutes(app, db);
	
	app.port(3000).run();
	
	
	return 0;
}
