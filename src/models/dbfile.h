// generated by ddl2cpp dbfile.sql dbfile DBFile
#ifndef DBFILE_DBFILE_H
#define DBFILE_DBFILE_H
#define CROW_JSON_USE_MAP

#include <sqlpp11/table.h>
#include <sqlpp11/data_types.h>
#include <sqlpp11/char_sequence.h>
#include <crow_all.h>
#include "../utilities/guid.h"

namespace DBFile
{
  namespace Dbfile_
  {
    struct Masterid
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "masterid";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T masterid;
            T& operator()() { return masterid; }
            const T& operator()() const { return masterid; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::blob, sqlpp::tag::require_insert>;
    };
    struct Id
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T id;
            T& operator()() { return id; }
            const T& operator()() const { return id; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::integer, sqlpp::tag::must_not_insert, sqlpp::tag::must_not_update>;
    };
    struct Guid
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "guid";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T guid;
            T& operator()() { return guid; }
            const T& operator()() const { return guid; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::blob, sqlpp::tag::require_insert>;
    };
    struct Filename
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "filename";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T filename;
            T& operator()() { return filename; }
            const T& operator()() const { return filename; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::varchar, sqlpp::tag::can_be_null>;
    };
    struct Filetype
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "filetype";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T filetype;
            T& operator()() { return filetype; }
            const T& operator()() const { return filetype; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::varchar, sqlpp::tag::can_be_null>;
    };
  } // namespace Dbfile_

  struct DBFile: sqlpp::table_t<DBFile,
               Dbfile_::Masterid,
               Dbfile_::Id,
               Dbfile_::Guid,
               Dbfile_::Filename,
               Dbfile_::Filetype>
  {
    struct _alias_t
    {
      static constexpr const char _literal[] =  "dbfile";
      using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
      template<typename T>
      struct _member_t
      {
        T dbfile;
        T& operator()() { return dbfile; }
        const T& operator()() const { return dbfile; }
      };
    };

    template <typename Row>
    static crow::json::wvalue get_row_as_json(Row& row)
    {
        crow::json::wvalue toReturn;
        toReturn["masterid"] = GUID::GUIDToString(row.masterid);
        toReturn["id"] = row.id;
        toReturn["guid"] = GUID::GUIDToString(row.guid);
        toReturn["filename"] = row.filename;
        toReturn["filetype"] = row.filetype;
        return toReturn;
    }
  };
} // namespace DBFile
#endif
