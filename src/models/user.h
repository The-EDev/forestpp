// generated by ddl2cpp user.sql user User
#ifndef USER_USER_H
#define USER_USER_H
#define CROW_JSON_USE_MAP

#include <sqlpp11/table.h>
#include <sqlpp11/data_types.h>
#include <sqlpp11/char_sequence.h>
#include <crow_all.h>
#include "../utilities/guid.h"

namespace User
{
  namespace User_
  {
    struct Id
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T id;
            T& operator()() { return id; }
            const T& operator()() const { return id; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::integer, sqlpp::tag::must_not_insert, sqlpp::tag::must_not_update>;
    };
    struct Guid
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "guid";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T guid;
            T& operator()() { return guid; }
            const T& operator()() const { return guid; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::blob, sqlpp::tag::require_insert>;
    };
    struct Username
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "username";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T username;
            T& operator()() { return username; }
            const T& operator()() const { return username; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::varchar, sqlpp::tag::require_insert>;
    };
    struct PasswordHash
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "password_hash";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T passwordHash;
            T& operator()() { return passwordHash; }
            const T& operator()() const { return passwordHash; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::varchar, sqlpp::tag::require_insert>;
    };
    struct Email
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "email";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T email;
            T& operator()() { return email; }
            const T& operator()() const { return email; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::varchar, sqlpp::tag::can_be_null>;
    };
    struct AuthTokenId
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "auth_token_id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T authTokenId;
            T& operator()() { return authTokenId; }
            const T& operator()() const { return authTokenId; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::integer, sqlpp::tag::can_be_null>;
    };
  } // namespace User_

  struct User: sqlpp::table_t<User,
               User_::Id,
               User_::Guid,
               User_::Username,
               User_::PasswordHash,
               User_::Email,
               User_::AuthTokenId>
  {
    struct _alias_t
    {
      static constexpr const char _literal[] =  "user";
      using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
      template<typename T>
      struct _member_t
      {
        T user;
        T& operator()() { return user; }
        const T& operator()() const { return user; }
      };
    };

    template <typename Row>
    static crow::json::wvalue get_row_as_json(const Row& row)
    {
        crow::json::wvalue toReturn;
        toReturn["id"] = row.id;
        toReturn["guid"] = GUID::GUIDToString(row.guid);
        toReturn["username"] = row.username;
        toReturn["email"] = row.email;
        return toReturn;
    }
  };

} // namespace User
#endif
