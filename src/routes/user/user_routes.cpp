#include <string>
#include <iostream>
#include <sqlpp11/sqlpp11.h>
#include <sqlpp11/sqlite3/sqlite3.h>
#include "user_routes.h"
#include "../../models/user.h"
#include "../../utilities/guid.h"
#include "../../utilities/hash.h"
#include "../../utilities/route_utilities.h"


void UserRoutes::getRoutes(crow::SimpleApp& app, sqlpp::sqlite3::connection& db){
	CROW_ROUTE(app, "/user")
    .methods("GET"_method, "POST"_method)
    ([&db](const crow::request& req, crow::response& res){

        User::User tab;

        if (req.method == crow::HTTPMethod::GET)
        {
            RouteUtilities::tableGet(req, res, db, tab);
            return;
        }

        crow::json::rvalue x;
		try{
        x = crow::json::load(req.body);
		}
		catch (...){
            res = crow::response(400, "probably json syntax error");
            res.end();
            return;
		}


        std::vector<crow::json::rvalue> y = x.lo();


        if (!x.has("username") || !x.has("password"))
        {
            res = crow::response(400, "malformed request");
            res.end();
            return;
        }
		
        std::string username (x["username"]);
		
		auto userCheck = db(select(tab.id).from(tab).where(tab.username == username));
		
		if (!userCheck.empty())
        {
            res = crow::response(400, "user exists");
            res.end();
            return;
        }
        std::string password (x["password"]);
        std::vector<std::uint8_t> guid = GUID::generateGUID();
		
		db(insert_into(tab).set(tab.username = username, tab.passwordHash = Hash::run_hkdf(password), tab.guid = guid));
        auto userRow = db(select(tab.id).from(tab).where(tab.username == username));
		
        crow::json::wvalue resBody;
        resBody["username"] = username;
        resBody["guid"] =  GUID::GUIDToString(guid);
        resBody["id"] = userRow.front().id;
		
        res = crow::response(201, resBody);
        res.end();
	});

    CROW_ROUTE(app, "/user/<int>").methods(crow::HTTPMethod::GET, crow::HTTPMethod::PATCH, crow::HTTPMethod::DELETE)
    ([&db](const crow::request& req, crow::response& res, int id){
        User::User tab;

        if (req.method == crow::HTTPMethod::PATCH)
        {
            RouteUtilities::tableUpdate(req, res, db, tab, id);
            return;
        }
        else if (req.method == crow::HTTPMethod::DELETE)
        {
            RouteUtilities::tableDelete(req, res, db, tab, id);
            return;
        }

        //Alias Substruct, take it's name function, and get the char* from that, then put the type of the table or member in there. and you got yourself a char* of the name.
        std::string whereThing (RouteUtilities::get_name<User::User>()+std::string(".id=")+std::to_string(id));

        auto userCheck = db(select(all_of(tab)).from(tab).where(sqlpp::verbatim<sqlpp::boolean>(whereThing)));

        if (userCheck.empty())
        {
            res = crow::response(404, "user not found");
            res.end();
            return;
}
        crow::json::wvalue x = User::User::get_row_as_json(userCheck.front());

        std::vector<std::string> y = req.url_params.keys();

        crow::json::wvalue xFinal = RouteUtilities::responseDataSuccess(x, y);

        res = crow::response(xFinal);
        res.end();
    });
}
