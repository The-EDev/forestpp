#pragma once
#include <sqlpp11/sqlite3/sqlite3.h>
#include "../routes.h"


class DBFileRoutes : public RouteCollection{
public:
    static void getRoutes(crow::SimpleApp& app, sqlpp::sqlite3::connection& db);
};

static inline std::string getFileNameFull(const std::unordered_map<std::string, std::string>& map);
