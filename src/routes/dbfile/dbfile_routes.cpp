#include <fstream>
#include <sys/stat.h>
#include "dbfile_routes.h"
#include "sqlpp11/sqlpp11.h"
#include "../../utilities/guid.h"
#include "../../utilities/token.h"
#include "../../models/dbfile.h"


inline std::string getFileNameFull(std::unordered_map<std::string, std::string>& map)
{
    for (auto pair : map)
    {
        if (pair.first == "filename")
        {
            return pair.second;
        }
    }
    return "";
}

inline void mkdirs(const std::string& item)
{
    std::string path("dbfile/" + item);
    mkdir("dbfile", 0777);
    mkdir(path.c_str(), 0777);
}

void DBFileRoutes::getRoutes(crow::SimpleApp& app, sqlpp::sqlite3::connection& db)
{
    CROW_ROUTE(app, "/dbfile/load").methods("GET"_method, "POST"_method)
    ([&db](const crow::request& req, crow::response& res)
    {
        crow::query_string params(req.url_params);

        int tokenID = Token::verifyToken(req, res, db);
        if (tokenID != -1)
        {

        if (req.method == crow::HTTPMethod::POST)
        {

            //TODO add function to automatically get params and return 400 if they are required and not found
            std::string masterid = params.pop("masterid");
            if (masterid == "")
            {
                res = crow::response(400, "Missing masterid parameter");
                res.end();
                return;
            }

            crow::multipart::message x(req);

            std::string fileName;
            std::string fileExt;
            std::vector<std::uint8_t> fileGUID(GUID::generateGUID());
            std::string fileGUIDString(GUID::GUIDToString(fileGUID));
            if (x.parts[0].headers[0].params.size() != 0) //TODO search for file in parts instead of taking first
            {
                std::string fnf(getFileNameFull(x.parts[0].headers[0].params));
                unsigned long found (fnf.find('.'));
                if (found == std::string::npos)
                {
                    res = crow::response(400, "No filename found");
                    res.end();
                    return;
                }
                fileName = std::string(fnf.substr(0, found));
                fileExt = std::string(fnf.substr(found+1));
            }

            mkdirs(masterid);

            std::string saveFilePath = "dbfile/" + masterid + '/' + fileGUIDString + '.' + fileExt;
            std::ofstream saveFile;
            saveFile.open(saveFilePath);
            saveFile << x.parts[0].body;
            saveFile.close();

            DBFile::DBFile tbl;

            int idResult = db(insert_into(tbl).set(tbl.filename = fileName, tbl.filetype = fileExt, tbl.guid = fileGUID, tbl.masterid = GUID::stringToGUID(masterid)));

            crow::json::wvalue resBody;
            resBody["masterid"] = masterid;
            resBody["guid"] = fileGUIDString;
            resBody["filename"] = fileName;
            resBody["filetype"] = fileExt;
            resBody["id"] = idResult;

            res = crow::response(201, resBody);
            res.end();
        }
        else
        {
            res.code = 405;
            res.end();
        }
        }
    });

    CROW_ROUTE(app, "/dbfile/load/<int>")([&db](const crow::request& req, crow::response& res, int itemID)
    {
        Token::verifyToken(req, res, db);

        DBFile::DBFile tbl;

        auto dbf = db(select(tbl.masterid, tbl.guid, tbl.filetype).from(tbl).where(tbl.id == itemID));

        if (dbf.empty())
        {
            CROW_LOG_INFO << "Could not find file with id " << itemID;
            res.code = 404;
            res.body = "file not found";
            res.end();
        }



        std::string path("dbfile/" + GUID::GUIDToString(dbf.front().masterid) + '/' + GUID::GUIDToString(dbf.front().guid) + '.' + std::string(dbf.front().filetype));
        CROW_LOG_INFO << path;
        res.set_static_file_info(path);
        res.end();

    });
}


