#pragma once
#include <string>
#include <sqlpp11/sqlite3/sqlite3.h>
#include "../routes.h"

class AuthRoutes: public RouteCollection{
public:
    static void getRoutes(crow::SimpleApp& app, sqlpp::sqlite3::connection& db);
private:
	static bool verifyPassword(std::string password, std::string originalHash);
};
