#include <sqlpp11/sqlpp11.h>
#include <ctime>
#include <sstream>
#include "../../models/user.h"
#include "../../models/auth_token.h"
#include "../../utilities/hash.h"
#include "../../utilities/token.h"
#include "auth_routes.h"

void AuthRoutes::getRoutes(crow::SimpleApp& app, sqlpp::sqlite3::connection& db){
	CROW_ROUTE(app, "/auth/login")
	.methods("POST"_method)
    ([&db](const crow::request& req){
                crow::json::rvalue x;
		try{
                        x = crow::json::load(req.body);
		}
		catch (...){
			return crow::response(400, "probably json syntax error");
		}
                if (!x.has("grant_type"))
            return crow::response(400);
		
                std::string grant_type (x["grant_type"]);
		
		AuthToken::AuthToken tokenTab;
		if (grant_type == "refresh_token"){
			
                        if (x.has("refresh_token")){
                                auto dbresult = db(select(all_of(tokenTab)).from(tokenTab).where(tokenTab.refreshToken == std::string(x["refresh_token"])));
                if (!dbresult.empty() && !dbresult.front().revoked){
				std::string access_token = Token::generateToken();
				std::string refresh_token = Token::generateToken();
				int issued = int(std::time(0));
                                db(update(tokenTab).set(tokenTab.accessToken = access_token, tokenTab.refreshToken = refresh_token, tokenTab.issue = issued, tokenTab.revoked = false).where(tokenTab.refreshToken == std::string(x["refresh_token"])));
                                crow::json::wvalue resBody;
				resBody["access_token"] = access_token;
				resBody["refresh_token"] = refresh_token;
				resBody["valid_for"] = 86400;
                                resBody["token_type"] = "bearer";

                return crow::response(resBody);
				}
                return crow::response(401, "invalid token");
			}
			
			return crow::response(400, "malformed request");
		}
		else if (grant_type == "password"){
                        if (x.has("username") && x.has("password")){
				User::User userTab;
				
                                std::string username (x["username"]);
                                std::string password (x["password"]);
				
				auto userResult =  db(select(userTab.id, userTab.username, userTab.passwordHash).from(userTab).where(userTab.username == username));
				if (!userResult.empty()){
					const auto& row = userResult.front();
					if (AuthRoutes::verifyPassword(password, row.passwordHash)){
						bool tokenFound = false;
						int userid = row.id;
						bool tokenInvalid = false;
						std::string access_token;
						std::string refresh_token;
						int issued;
						int expiresIn;
						auto tokenResult = db(select(all_of(tokenTab)).from(tokenTab).where(tokenTab.userId == userid));
						
						if (!tokenResult.empty()){
							const auto& tokenRow = tokenResult.front();
							tokenFound = true;
							int timeNow  = int(std::time(0));
							int expiretime = tokenRow.issue + tokenRow.validFor;
							if (tokenRow.revoked == true || expiretime < timeNow){
								tokenInvalid = true;
							}
							else {
							access_token = tokenRow.accessToken;
							refresh_token = tokenRow.refreshToken;
							issued = tokenRow.issue;
							expiresIn = tokenRow.issue + tokenRow.validFor - timeNow;
							}
						}
						if (tokenInvalid || !tokenFound){
						access_token = Token::generateToken();
						refresh_token = Token::generateToken();
						issued = int(std::time(0));
						expiresIn = 86400;
						}
						if (tokenFound && tokenInvalid){
                            db(update(tokenTab).set(tokenTab.accessToken = access_token, tokenTab.refreshToken = refresh_token, tokenTab.issue = issued, tokenTab.revoked = false).where(tokenTab.userId == userid));
						}
						else if (!tokenFound) {
                            //TODO change this to normal insert rather than multi_insert
						auto multi_insert = insert_into(tokenTab).columns(tokenTab.accessToken, tokenTab.refreshToken, tokenTab.issue, tokenTab.revoked, tokenTab.userId);
						multi_insert.values.add(tokenTab.accessToken = access_token, tokenTab.refreshToken = refresh_token, tokenTab.issue = issued, tokenTab.revoked = false, tokenTab.userId = userid);
						db(multi_insert);
						}
                                                crow::json::wvalue resBody;
						resBody["access_token"] = access_token;
						resBody["refresh_token"] = refresh_token;
						resBody["valid_for"] = expiresIn;
                                                resBody["token_type"] = "bearer";
						
                                                crow::response res(resBody);
                                                //res.set_header("Content-Type", "application/json");
						return res;
					}
					else {
						return crow::response(401, "incorrect password");
					}
				}
				return crow::response(401, "incorrect username");
			}
		}
		

		
		
		return crow::response(400, "malformed request");
	});

    CROW_ROUTE(app, "/auth/logout")
    ([&db](const crow::request& req, crow::response& res)
    {
        int tokenID = Token::verifyToken(req, res, db, "Bearer", true);
        AuthToken::AuthToken tokenTab;
        if (tokenID == -1)
        {
            res = crow::response(401, "missing, malformed, or invalid token");
            res.end();
        }
        db(update(tokenTab).set(tokenTab.revoked = true).where(tokenTab.id == tokenID));

        res.end();
    });
}

bool AuthRoutes::verifyPassword(std::string password, std::string originalHash){
	std::string passwordHash = Hash::run_hkdf(password);
	return passwordHash == originalHash;
}
